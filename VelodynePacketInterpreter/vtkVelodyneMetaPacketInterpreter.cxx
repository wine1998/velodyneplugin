#include "vtkVelodyneMetaPacketInterpreter.h"

#include <vtkObjectFactory.h>

vtkStandardNewMacro(vtkVelodyneMetaPacketInterpreter)

//-----------------------------------------------------------------------------
vtkVelodyneMetaPacketInterpreter::vtkVelodyneMetaPacketInterpreter()
{
  this->PotentialInterps->AddItem(vtkVelodyneLegacyPacketInterpreter::New());
  this->PotentialInterps->AddItem(vtkVelodyneAdvancedPacketInterpreter::New());
  #ifdef VELOVIEW_HAS_VELARRAY
  this->PotentialInterps->AddItem(vtkVelodyneSpecialVelarrayPacketInterpreter::New());
  #endif

  this->LaserSelection->SetNumberOfTuples(HDL_MAX_NUM_LASERS);
  this->LaserSelection->Fill(static_cast<int>(true));
}

//-----------------------------------------------------------------------------
vtkSmartPointer<vtkPolyData> vtkVelodyneMetaPacketInterpreter::CreateNewEmptyFrame(
  vtkIdType vtkNotUsed(numberOfPoints), vtkIdType vtkNotUsed(prereservedNumberOfPoints))
{
  assert(false && "this function should never be called");
  return vtkSmartPointer<vtkPolyData>::New();
}

//-----------------------------------------------------------------------------
void vtkVelodyneMetaPacketInterpreter::LoadCalibration(const std::string &filename)
{
  if (this->SelectedInterp)
  {
    this->SelectedInterp->LoadCalibration(filename);
  }
  else
  {
    for (int i = 0; i < this->PotentialInterps->GetNumberOfItems(); ++i)
    {
      vtkVelodyneBasePacketInterpreter* pt = vtkVelodyneBasePacketInterpreter::SafeDownCast(this->PotentialInterps->GetItemAsObject(i));
      pt->LoadCalibration(filename);
    }
  }
}

//-----------------------------------------------------------------------------
bool vtkVelodyneMetaPacketInterpreter::IsLidarPacket(const unsigned char *data, unsigned int dataLength)
{
  if (this->SelectedInterp)
  {
    return this->SelectedInterp->IsLidarPacket(data, dataLength);
  }
  else
  {
    for (int i = 0; i < this->PotentialInterps->GetNumberOfItems(); ++i)
    {
      vtkVelodyneBasePacketInterpreter* pt = vtkVelodyneBasePacketInterpreter::SafeDownCast(this->PotentialInterps->GetItemAsObject(i));
      if (pt->IsLidarPacket(data, dataLength))
      {
        if (!this->SelectedInterp)
        {
          this->SelectedInterp = pt;
          return true;
        }
        else
        {
          vtkErrorMacro("Multiple Interpreter could interpret this packet! According to the specification, this is impossible!")
          return false;
        }
      }
    }
    return false;
  }
}

//-----------------------------------------------------------------------------
void vtkVelodyneMetaPacketInterpreter::ResetCurrentFrame()
{
  if (this->SelectedInterp != nullptr)
  {
    this->SelectedInterp->ResetCurrentFrame();
  }
  else
  {
    for (int i = 0; i < this->PotentialInterps->GetNumberOfItems(); ++i)
    {
      vtkVelodyneBasePacketInterpreter* pt = vtkVelodyneBasePacketInterpreter::SafeDownCast(this->PotentialInterps->GetItemAsObject(i));
      pt->ResetCurrentFrame();
    }
  }
}

//-----------------------------------------------------------------------------
void vtkVelodyneMetaPacketInterpreter::ClearAllFramesAvailable()
{
  if (this->SelectedInterp != nullptr)
  {
    this->SelectedInterp->ClearAllFramesAvailable();
  }
  else
  {
    for (int i = 0; i < this->PotentialInterps->GetNumberOfItems(); ++i)
    {
      vtkVelodyneBasePacketInterpreter* pt = vtkVelodyneBasePacketInterpreter::SafeDownCast(this->PotentialInterps->GetItemAsObject(i));
      pt->ClearAllFramesAvailable();
    }
  }
}

//-----------------------------------------------------------------------------
std::string vtkVelodyneMetaPacketInterpreter::GetSensorInformation(bool shortVersion)
{
  if (!this->SelectedInterp)
  {
    return "Could not determine the interpreter for now";
  }
  else
  {
    return this->SelectedInterp->GetSensorInformation(shortVersion);
  }
}

//-----------------------------------------------------------------------------
std::string vtkVelodyneMetaPacketInterpreter::GetDefaultRecordFileName()
{
  if (!this->SelectedInterp)
  {
    // YYYY-mm-dd-HH-MM-SS_Velodyne-Data
    return vtkLidarPacketInterpreter::GetDefaultRecordFileName() + "_Velodyne-Data";
  }
  else
  {
    // YYYY-mm-dd-HH-MM-SS_Velodyne-SensorName-Data
    return this->SelectedInterp->GetDefaultRecordFileName();
  }
}

//-----------------------------------------------------------------------------
void vtkVelodyneMetaPacketInterpreter::ResetParserMetaData()
{
  if (this->SelectedInterp != nullptr)
  {
    this->SelectedInterp->ResetParserMetaData();
  }
  else
  {
    for (int i = 0; i < this->PotentialInterps->GetNumberOfItems(); ++i)
    {
      vtkVelodyneBasePacketInterpreter* pt = vtkVelodyneBasePacketInterpreter::SafeDownCast(this->PotentialInterps->GetItemAsObject(i));
      pt->ResetParserMetaData();
    }
  }
}

//-----------------------------------------------------------------------------
void vtkVelodyneMetaPacketInterpreter::SetParserMetaData(const FrameInformation &metaData)
{
  if (this->SelectedInterp != nullptr)
  {
    this->SelectedInterp->SetParserMetaData(metaData);
  }
  else
  {
    for (int i = 0; i < this->PotentialInterps->GetNumberOfItems(); ++i)
    {
      vtkVelodyneBasePacketInterpreter* pt = vtkVelodyneBasePacketInterpreter::SafeDownCast(this->PotentialInterps->GetItemAsObject(i));
      pt->SetParserMetaData(metaData);
    }
  }
}

//-----------------------------------------------------------------------------
void vtkVelodyneMetaPacketInterpreter::SetLaserSelection(int index, int value)
{
  if (this->SelectedInterp != nullptr)
  {

    this->SelectedInterp->SetLaserSelection(index, value);
  }
  else
  {
    for (int i = 0; i < this->PotentialInterps->GetNumberOfItems(); ++i)
    {
      vtkVelodyneBasePacketInterpreter* pt = vtkVelodyneBasePacketInterpreter::SafeDownCast(this->PotentialInterps->GetItemAsObject(i));
      pt->SetLaserSelection(index, value);
    }
  }
}

//-----------------------------------------------------------------------------
vtkIntArray* vtkVelodyneMetaPacketInterpreter::GetLaserSelection()
{
  if (this->SelectedInterp != nullptr)
  {
    return this->SelectedInterp->GetLaserSelection();
  }
  else
  {
    for (int i = 0; i < this->PotentialInterps->GetNumberOfItems(); ++i)
    {
      vtkVelodyneBasePacketInterpreter* pt = vtkVelodyneBasePacketInterpreter::SafeDownCast(this->PotentialInterps->GetItemAsObject(i));
      return pt->GetLaserSelection();
    }
  }

  return nullptr;
}

//-----------------------------------------------------------------------------
void vtkVelodyneMetaPacketInterpreter::SetCropRegion(double _arg1, double _arg2, double _arg3, double _arg4, double _arg5, double _arg6)
{
  if (this->SelectedInterp != nullptr)
  {
    this->SelectedInterp->SetCropRegion(_arg1, _arg2, _arg3, _arg4, _arg5, _arg6);
  }
  else
  {
    for (int i = 0; i < this->PotentialInterps->GetNumberOfItems(); ++i)
    {
      vtkVelodyneBasePacketInterpreter* pt = vtkVelodyneBasePacketInterpreter::SafeDownCast(this->PotentialInterps->GetItemAsObject(i));
      pt->SetCropRegion(_arg1, _arg2, _arg3, _arg4, _arg5, _arg6);
    }
  }
}

//-----------------------------------------------------------------------------
void vtkVelodyneMetaPacketInterpreter::GetXMLColorTable(double XMLColorTable[])
{
  if (this->SelectedInterp)
  {
   this->SelectedInterp->GetXMLColorTable(XMLColorTable);
  }
  else
  {
    for (int i = 0; i < this->PotentialInterps->GetNumberOfItems(); ++i)
    {
      vtkVelodyneBasePacketInterpreter* pt = vtkVelodyneBasePacketInterpreter::SafeDownCast(this->PotentialInterps->GetItemAsObject(i));
      pt->GetXMLColorTable(XMLColorTable);
    }
  }
}

//-----------------------------------------------------------------------------
void vtkVelodyneMetaPacketInterpreter::GetLaserCorrections(double verticalCorrection[], double rotationalCorrection[], double distanceCorrection[], double distanceCorrectionX[], double distanceCorrectionY[], double verticalOffsetCorrection[], double horizontalOffsetCorrection[], double focalDistance[], double focalSlope[], double minIntensity[], double maxIntensity[])
{
  if (this->SelectedInterp)
  {
    this->SelectedInterp->GetLaserCorrections(verticalCorrection, rotationalCorrection, distanceCorrection, distanceCorrectionX, distanceCorrectionY, verticalOffsetCorrection, horizontalOffsetCorrection, focalDistance, focalSlope, minIntensity, maxIntensity);
  }
  else
  {
    for (int i = 0; i < this->PotentialInterps->GetNumberOfItems(); ++i)
    {
      vtkVelodyneBasePacketInterpreter* pt = vtkVelodyneBasePacketInterpreter::SafeDownCast(this->PotentialInterps->GetItemAsObject(i));
      pt->GetLaserCorrections(verticalCorrection, rotationalCorrection, distanceCorrection, distanceCorrectionX, distanceCorrectionY, verticalOffsetCorrection, horizontalOffsetCorrection, focalDistance, focalSlope, minIntensity, maxIntensity);
    }
  }
}

//-----------------------------------------------------------------------------
vtkMTimeType vtkVelodyneMetaPacketInterpreter::GetMTime()
{
  vtkMTimeType time = this->Superclass::GetMTime();
  for (int i = 0; i < this->PotentialInterps->GetNumberOfItems(); ++i)
  {
    time = std::max(time, this->PotentialInterps->GetItemAsObject(i)->GetMTime());
  }
  return time;
}
