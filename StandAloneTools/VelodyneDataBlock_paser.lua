-- trivial protocol example
-- declare our protocol
trivial_proto = Proto("VelodyneDataBlocks","Velodyne Data blocks")
-- create a function to dissect it
function trivial_proto.dissector(buffer,pinfo,tree)
	pinfo.cols.protocol = "VelodyneDataBlocks"

	local subtree = tree:add(trivial_proto,buffer(),"Velodyne Data blocks :" ..buffer:len())

	local curr = 0

  -- Check packet size --
	local goodSize = 1248 - 42
	local totalSize = buffer:len()
	if totalSize ~= goodSize then return end


	---- Data Blocks ----

	local nbDataBlock = 12
	local sizeDataBlock = 100
	local DataBlockSize = nbDataBlock*sizeDataBlock
	local nbChannels = 32


	local datablocks = subtree:add(buffer(curr, DataBlockSize),"Data blocks")

	for i=1, nbDataBlock
	do
		local dataBlock_subtree = datablocks:add(buffer(curr, sizeDataBlock),"Data Block " ..i)

		local Flag = buffer(curr,2):uint()
		dataBlock_subtree:add(buffer(curr,2),"Flag  : " .. Flag)
		curr = curr+2

		local Azimuth = buffer(curr,2):uint()
		dataBlock_subtree:add(buffer(curr,2),"Azimuth  : " .. Azimuth)
		curr = curr+2

		local channels = dataBlock_subtree:add(buffer(curr, 3*nbChannels),"Channels")

		---- Channels  ----
		for c=0, nbChannels-1
		do
			numChannel = c % 16
			local channel_subtree = channels:add(buffer(curr, 3),"Channel " ..numChannel)

			local Distance = buffer(curr,2):uint()
			channel_subtree:add(buffer(curr,2),"Distance  : " .. Distance)
			curr = curr + 2

			local Reflectivity = buffer(curr,1):uint()
			channel_subtree:add(buffer(curr,1),"Reflectivity  : " .. Reflectivity)
			curr = curr + 1
		end

	end

	-- Footer --

	local TimeStamp = buffer(curr, 4):uint()
	subtree:add(buffer(curr,4),"TimeStamp   : " .. TimeStamp)
	curr = curr + 4

	local Factory = buffer(curr, 2):uint()
	subtree:add(buffer(curr,2),"Factory   : " .. Factory)
	curr = curr + 2

end


-- load the udp.port table
udp_table = DissectorTable.get("udp.port")
-- register our protocol to handle udp port
udp_table:add(2368,trivial_proto)
